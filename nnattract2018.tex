%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\documentclass[a4paper,11pt,oneside, english]{article}
\usepackage[a4paper,bindingoffset=0cm,left=2.5cm,right=2.5cm,top=1.5cm,bottom=2.2cm,width=16cm,footskip=0.5cm]{geometry}

\usepackage{eurosym}
%\input{pool/NextLatexPackagesBase.tex}
%\input{pool/NextDefs.tex}

\usepackage[utf8]{inputenc}
\usepackage{enumitem}
\setitemize{noitemsep,topsep=0pt,parsep=0pt,partopsep=0pt}
\usepackage{fancyhdr}
%%% Added to help mimic structure.
\usepackage{tcolorbox}
\usepackage[explicit]{titlesec}
\usepackage{lastpage}
\usepackage[normalem]{ulem}

\usepackage{hyperref} %,xcolor,graphicx,ifpdf,wrapfig,fancyhdr,lastpage,etoolbox,tocloft,multirow,array,pdflscape,enumitem,eurosym,amssymb,titlesec,multirow,footnote,color,colortbl,scrextend,etoolbox}
\hypersetup{colorlinks,%
	citecolor=blue,%
	linkcolor=blue,%
	filecolor=black,%
	urlcolor=blue}

%%% HEADER
\setlength{\headheight}{1cm}
\setlength{\headsep}{0.7cm}
\pagestyle{fancyplain}
\fancyheadoffset[HL]{2cm}
\fancyheadoffset[HR]{1.5cm}
\fancyhf{}

\cfoot{\thepage\ de \pageref{LastPage} }
\renewcommand{\headrulewidth}{0pt} % remove lines
\renewcommand{\footrulewidth}{0pt}

\renewcommand\thesection{\arabic{section}.}
\renewcommand\thesubsection{\arabic{section}.\arabic{subsection}}
\titleformat{\section}{\large\bfseries}{\thesection}{0.2em}{\uline{#1}}


\begin{document}
\begin{figure}[htp!]
\centering
\includegraphics[width=0.5\textwidth]{attract.png}
\end{figure} 

\begin{center}
\large
{Proposal Title and Acronym} \\ 
\textbf{Encoder-Decoder Online Compression (EDOC)}\\
\vspace{0.5cm}
\end{center}

\begin{center}
\large
{This proposal responds to challenge(s) in the following domain(s)} \\ 
\textbf{Sensors/ front and back end electronics/ data acquisition systems and computing/ software and integration}\\
\vspace{0.5cm}
\end{center}

\begin{center}
\large
{Coordinator's name and email address} \\ 
\textbf{Vicente Herrero-Bosch (viherbos@eln.upv.es)}\\
\vspace{0.5cm}
\end{center}

\begin{center}
\large
\textbf{Consortium Composition Table} \\ 
\vspace{0.1cm}
\end{center}
\begin{table}[hp!]
%\caption{default}
\begin{center}
\begin{tabular}{|c|c|c|c|c|c|}
\hline
& Organization & Organization  & Organization & Contact  & Contact person  \\
& full  & short name / & type & person  & email \\
& name & PIC &   & name  & \\
& & number & & & \\
& & & & & \\
\hline
Coordinator & Universitat  & UPV & University  & Vicente  & viherbos@eln.upv.es\\
& Polit\`{e}cnica  &  999864846 & & Herrero- & \\
& de Val\`{e}ncia & & & Bosch &  \\
& & & & & \\ 
\hline
Partner 2 & Universidade de & USC & University  & Jos\'{e} \'{A}ngel & Jose.Angel.\\
& Santiago de & 999829635 & & Hernando & Hernando-\\
& Compostela & &  & Morata & Morata@cern.ch\\
\hline
Partner 3 & Bernia & & Startup & Alfonso & \\
& Molecular & Bernia & & Rios &\\
& Imaging & & & & \\
\hline
\end{tabular}
\end{center}
\label{default}
\end{table}%
\newpage

\section*{Project Public Summary}
Electronic readout for large-scale detector systems becomes more challenging as the number of sensors increases. The huge amount of information generated as well as the high speed required in the data links are some of the most important drawbacks in the next generation readout systems. Therefore inline data processing could help to ease the storage and speed requirements. For instance an efficient compression system could reduce by a factor of 10 the amount of data being handled and stored by a multi-sensor readout scheme.

We envision an encoder-decoder architecture system with a hardware-based implementation inside the readout, and in which the decoder can be implemented as a part of the software offline processing. The hardware implementation will make use of an autoencoder neural-network (NN) based compression scheme. This specific type of NN can be trained on-board in an unsupervised fashion simply by using input data from the detector, since the target after the encoding-decoding process is the input signal itself. The resulting encoded signals could be transmitted through the readout along with a value representing the quality of the compression and the decoder parameters. This information should be enough to recover the original inputs offline with a controlled degree of accuracy which could be improved over time with new online fine tuning training operations in the autoencoder NN.

Since the use of an inline processing element will introduce an extra latency in the data transmission, a careful timing simulation will be required in order to establish the maximum value of this latency as well as the memory buffers needed to cope with the data rate specifications of the detector. This simulation will also allow us to decide the best place to introduce the encoder element so that its effects are maximized.

Although NNs have been applied to data compression for a considerable time, the potential of a hardware implementation that can be trained online inside a readout system has not been sufficiently developed. Introducing this element in large detector systems could help to reduce the problems related to handling and storing the huge amounts of data being generated.
\newpage

\section{Project Description}
Detectors containing a large number of sensors and requiring fast readout are rapidly becoming more common, especially in particle physics and medical applications.  New designs employ increasing numbers of readout channels, as detectors with more sensors often achieve greater geometrical coverage and can detect weaker signals with improved resolution.  This presents new challenges in readout electronics and data processing which the proposed project seeks to address using a neural-network-based approach to data compression.\\

\noindent\textbf{\textbullet\,\,Neural Networks}\\
Neural networks have, in recent years, provided some of the best solutions to difficult problems such as speech\footnote{G. Hinton et al., Deep neural networks for acoustic modeling in speech recognition, IEEE Signal Processing Magazine 29 (2012) 82.} and image\footnote{C. Szegedy, W. Liu, Y. Jia, P. Sermanet, S. E. Reed, D. Anguelov et al., Going deeper with convolutions, arXiv:1409.4842.} recognition.  Several important advances have contributed to this, including faster machines, acceleration of training and execution algorithms using specialized hardware, and intense research in network design and optimization.  Specifically, the advent of the ``deep learning'' era, in which neural networks with neurons arranged in many layers stacked one after the other (or ``deep neural networks,'' DNNs) have shown great promise in solving complex problems, has sparked broad interest in developing tools and hardware for making use of such networks in a wide range of applications.

A neural network is built from individual units called \emph{neurons}, each of which is given some number of inputs $x_i$ and yield an output $y$ according to its parameters, which include \emph{weights} $w_{i}$, one per input $i$, and a \emph{bias} $b$.  The output value is computed using the weights and bias according to an activation function $f$, as $y = f(\sum_{i}w_{i}x_{i} + b)$.  Neurons are arranged in layers, and the network is trained using a sample of training data which includes sets of inputs and their corresponding expected outputs.  The network ``learns'' via a training procedure in which the inputs are propagated through the entire network, and the resulting output values compared with the expected outputs via the computation of a \emph{loss function}.  For each training sample, the weights and biases of the neurons are adjusted in a way that minimizes the loss function in a procedure called back-propagation.  

\begin{figure}[!htb]
	\centering
	\includegraphics[width= 0.48\textwidth]{img/general_nn_fc.pdf}
	\includegraphics[width= 0.48\textwidth]{img/cnn_example.pdf}
	\caption{(Left) A fully-connected neural network architecture.  Here the outputs of each neuron are connected as inputs to every neuron in the next layer.  (Right) A convolutional layer.  Each neuron in the layer considers as input only neurons in a region of size $m \times n ~(\times ~k_i)$, where $m \times n$ is the ``filter size'' and $k_i$ is the number of input channels. The $k_f$ output channels correspond roughly to the number of ``features'' to be learned from the previous layer.  The neurons in each output channel share the same set of $m \times n ~(\times ~k_i)$ weights and a bias.  Sharing of weights and biases serves to significantly reduce the total number of parameters in the network in comparison to fully-connected layers.  (Some content of figure from \href{http://dx.doi.org/10.1088/1748-0221/12/01/T01004}{JINST 12 (2017) T01004.})} \label{fig.nets}
\end{figure}

Neural networks are designed according to various architectures that attempt to match the problem at hand.  They often contain ``fully connected'' layers, in which the output of each neuron in the previous layer is connected to each input neuron, or specially designed layers such as ``convolutional'' layers.  A convolutional neural network (CNN) contains one or more convolutional layers, in which the inputs to each neuron consist of only a subset of the outputs in the previous layer, and the outputs values are constructed using weights and biases that are shared across multiple input neurons.  Each set of outputs sharing a common set of weights and a bias is called a ``feature map,'' as it can be roughly thought to identify with a specific ``feature'' of the input data.  Fig. \ref{fig.nets} describes these concepts in more detail.\\

\noindent\textbf{\textbullet\,\,Data compression with an encoder-decoder network}\\
An encoder-decoder network is one in which some number of input values are compressed (encoded) into a smaller number of values, which can later be decompressed (decoded) into the original values.  A proposed scheme is shown in Fig. \ref{fig.encdec}.  It consists of a large neural network in which the relevant signals are inputs and are reduced to fewer values by some chosen network architecture, possibly consisting of several fully-connected and convolutional layers.  These reduced values must then be decoded into the original inputs.

The idea would be to implement such a scheme in hardware, so that the user could plug in the input signals, train the network on-board until an acceptable quality factor is achieved, and then proceed to save only the encoded signals and corresponding quality factors.  The weights and biases of the trained decoder would also be saved to later reconstruct the full set of signals offline.  

\begin{figure}[!htb]
	\centering
	\includegraphics[width= 1.0\textwidth]{img/enc_dec_scheme.pdf}
	\caption{Proposed scheme for encoder-decoder neural-network-based compression.  The input signals are provided as both the inputs and the target values, and a neural network is trained to reduce the input information into a smaller number of values (encoder) and rebuild it again (decoder).  The reduced values can be saved to disk as well as the decoder parameters so that the original inputs can be reconstructed offline.  While the true target information is available, a ``quality factor'' that describes how well the decoded signals match the originals can also be recorded to later provide a measure of the losses introduced by the compression.} \label{fig.encdec}
\end{figure}

\noindent\textbf{\textbullet\,\,Project objectives}
\begin{enumerate}[noitemsep, leftmargin=10pt, topsep=5pt]
	\item[1.] \textbf{Software proof-of-concept}: The idea will first need to be demonstrated with a software-based implementation.  Many tools are available for designing, training, and evaluating neural networks with GPU-based acceleration, such as Keras\footnote{F. Chollet, ``Keras.'' \href{https://github.com/fchollet/keras}{https://github.com/fchollet/keras}, 2015. } and Tensorflow\footnote{M. Abadi, A. Agarwal, P. Barham, E. Brevdo, Z. Chen, C. Citro et al., TensorFlow: Large-scale machine learning on heterogeneous systems, \href{https://www.tensorflow.org}{https://www.tensorflow.org}, 2015.}.  A network that successfully compresses the chosen number of signals can be designed and optimized.  Factors such as the input size and complexity of the network should be considered and chosen reasonably to fit the selected hardware components on which the design will be implemented.
	\item[2.] \textbf{Hardware proof-of-concept}: The designed system could be implemented on a PCIe board such as the XpressGXA10-LP1150\footnote{REFLEX CES: \href{https://www.reflexces.com/products-solutions/cots-boards/pci-express/xpressgxa10-lp1150}{https://www.reflexces.com/products-solutions/cots-boards/pci-express/xpressgxa10-lp1150}} (see Fig. \ref{fig.PCIe}), containing a modern FPGA, integrated into a Linux-based machine.  High-level languages such as OpenCL allow for fast implementation of neural network architectures on FPGAs.  In perhaps the most challenging part of the project, a training procedure based on back-propagation must be carried out in a reasonable amount of time on-board.  
	\item[3.] \textbf{Intermediate-scale demonstrator}: The final goal would be to produce a demonstrator capable of reading out several hundred signals. Latency and throughput specifications should be compatible with a high rate operation of the associated detector. A possible candidate application could be a modern intermediate-scale PET detector. The usual radioisotope dosages used in this type of scanner, on the order of 300 MBq, would be enough to stress the demonstrator and test its capabilities.
\end{enumerate}

\begin{figure}[!htb]
	\centering
	\begin{minipage}[c]{0.50\textwidth}
		\frame{\includegraphics[width= 1.0\textwidth]{img/XpressGXA10_LP1150.png}}
	\end{minipage}\hfill
	\begin{minipage}[c]{0.45\textwidth}
		\caption{(from \href{https://www.reflexces.com/products-solutions/cots-boards/pci-express/xpressgxa10-lp1150}{REFLEX CES}) The XpressGXA10-LP1150 PCIe board with built-in Intel Arria 10 GX FPGA.  A hardware implementation of the encoder-decoder-based compression could be realized on this board using OpenCL.} \label{fig.PCIe}
	\end{minipage}
\end{figure}
 
\section{Technology Benchmark}
Online training with FPGA-based acceleration has already been implemented\footnote{Pinjare, S.L.; Arun Kumar, M. Implementation of Neural Network Back Propagation Training Algorithm on FPGA. Int. J. Comput. Appl. 52, 1 (2012).} using complex custom developed processing elements. However this type of implementation has an intrinsic lack of flexibility that makes it difficult to upgrade the system in order to take advantage of technological changes. From the performance point of view, GPU-based implementations deliver more computational power, but bandwidth limits in data transfer operations show up as their main bottleneck. In order to establish a trade-off between flexibility and performance, new tools have been developed that allow for a distributed implementation of the ANN and its training system in different hardware platforms. Usually FPGAs host fine-grained kernels that cooperate with CPUs to carry out specialized operations in a processor-coprocessor fashion.\footnote{Qiao, Y; Shen, J. et al. Optimizing OpenCL Implementation of Deep Convolutional Neural Network on FPGA. Network and Parallel Computing 2017}\textsuperscript{,}\footnote{Gadea, R.; Colom-Palero, R. and Herrero-Bosch, V. Optimization of Deep Neural Networks using SoCs with OpenCL Sensors. MDPI 18 (2018)} Moreover, an OpenCL-based implementation allows one to overcome the flexibility issues formerly mentioned by introducing an abstraction layer in the design procedure.

\section{Envisioned Innovation Potential}

The proposed project aims to demonstrate the effectiveness of a new strategy to reduce data load in experiments storing large amounts of information.  Further investigation to make faster, more compact versions of the hardware developed could lead to widespread use of such a strategy in the next decade.  This would have a major industrial impact, as it would add a new dimension to electronic readout design in which a trainable device is first used to simplify vast amounts of data before saving to disk.  Further research could reveal how to extract valuable information from the characteristics of the data learned during the training process, that is, in addition to being more compact than the original inputs, the encoded inputs may even be more useful for performing the data analysis than the decoded information.  Such advances could lead to benefits for Europe and its citizens by inspiring new industrial ideas and opportunities. As stated in objective 3, one of the possible leading applications of this device would be to enhance medical imaging scanners, which could have a strong effect on the development of total body PET\footnote{Cherry, S.R.; Jones, T.; et al. Total-Body PET: Maximizing Sensitivity to Create New Opportunities for Clinical Research and Patient Care. J. Nucl. Med. 59 1 (2017)} systems in which the number of readout channels exceeds the capabilities of current data acquisition systems. A joint collaboration with a medical imaging startup company such as Bernia, our partner in this proposal, will allow us to test EDOC inside a real PET detector system.

%The proposed project aims to demonstrate the effectiveness of a new strategy to reduce data load in experiments storing large amounts of information.  Further investigation to make faster, more compact versions of the hardware developed could lead to widespread use of such a strategy in the next decade.  This would have a major industrial impact, as it would add a new dimension to electronic readout design in which a trainable device is first used to simplify vast amounts of data before saving to disk.  Further research could reveal how to extract valuable information from the characteristics of the data learned during the training process, that is, in addition to being more compact than the original inputs, the encoded inputs may even be more useful for performing the data analysis than the decoded information.  Such advances could lead to benefits for Europe and its citizens by inspiring new industrial ideas and opportunities, and permitting more precise medical devices with more sensors. 

\section{Project Implementation, Budget Breakdown and Final Deliverables}

The leading researchers involved in the project will be: V. Herrero-Bosch (UPV) who will act as coordinator; R. Gadea-Giron\'{e}s (UPV); J.A. Hernando-Morata (USC) and A. Rios (Bernia). The project will also support one postdoc at USC and a student at UPV.  The machines on which the software and on-board hardware (PCIe) implementations will be initially developed are already present at UPV.  When ready for testing, the final hardware prototype will be constructed on machines purchased by Bernia.  The budget is presented in Table \ref{tab.budget}.\\

\begin{table}[htp]
\caption{Budget}
\begin{center}
\begin{tabular}{|c|c|c|c|c|}
\hline
&\multicolumn{3}{|c|}{Cost in k\euro} \\ \hline

Item & UPV & USC & Bernia \\
\hline
\hline
Direct personnel \\ \hline
Postdoc & 0 & 40 & 0\\
Student & 22 & 0 & 0\\
\hline
\hline
Other direct \\ \hline
Equipment & 0 & 0 & 10 \\
\hline
Travel & 2 & 6 & 0 \\
\hline
Subcontracting & 0 & 0 & 0\\
\hline
\hline
Indirect costs (25\% of direct costs) & 6 & 11.5 & 2.5\\
\hline
\hline
 & 30 & 57.5 & 12.5	 \\
\hline

Total & \multicolumn{3}{|c|}{100} \\ \hline

\end{tabular}


\end{center}
\label{tab.budget}
\end{table}%


\noindent Final deliverables:
\begin{itemize}[noitemsep,topsep=0pt,parsep=0pt,partopsep=0pt]
\item \textbf{Intermediate-scale compression setup}: a functioning setup demonstrating the ability to compress a number of signals suitable for use in an intermediate-scale medical imaging procedure.
\item \textbf{Publications in international journals and congresses}: Due to the potential application of this project in different fields from experimental physics to medical imaging, a high impact is expected. The diffusion of its results will be of great importance in obtaining further funding towards the next stage of development.  
\item \textbf{Final project summary and poster}: To be presented during the ATTRACT final event.
 
\end{itemize}


%\section{Bibliography}
%\bibliographystyle{pool/NextRefsStyle}
%\bibliography{pool/NextRefs}

\end{document}
